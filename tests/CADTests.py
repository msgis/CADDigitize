#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import print_function

from qgis.core import QgsPoint
from CADShape.CADCircle import CADCircle
from CADShape.CADCircularArc import CADCircularArc
from CADShape.CADEllipse import CADEllipse
from CADShape.CADRectangle import CADRectangle
from CADShape.CADRegularPolygon import CADRegularPolygon


if __name__ == "__main__":
    C = QgsPoint(0, 0)
    B = QgsPoint(5, 5)

    rp = CADRegularPolygon(C, B, 5)

    points = [i for i in rp]

    rp2 = CADRegularPolygon.by2Corners(points[0], points[1], 5)
    points2 = [i for i in rp2]

    print(rp)
    print(points)
    print("------")
    print(rp2)
    print(points2)

    print(rp == rp2)
